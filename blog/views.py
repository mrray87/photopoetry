from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from .form import PostForm
from .models import Post, PostMedia, UserImage


def post_list(request):
    posts = Post.objects.all().order_by('published_date')
    return render(request, 'blog/post_list.html', {'posts': posts})


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post_media = PostMedia.objects.filter(post=post)[0]
    user_images = UserImage.objects.filter(post_media=post_media)
    post_images = []
    for image in user_images:
        print(image)
        post_images.append(image.image.url)
    print(post_images)
    return render(request, 'blog/post_detail.html', {'post': post, 'post_images': post_images})


def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST, request.FILES)

        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()

            if request.FILES['media']:
                post_media = PostMedia(post=post)
                post_media.save()

                for image in enumerate(request.FILES.getlist('media')):
                    user_image = UserImage(post_media=post_media, image=image[1])
                    user_image.save()

            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})
