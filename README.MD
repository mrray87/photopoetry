# TODO:
* ~~Display part of the text of the post on the dashboard~~
* ~~Add file upload~~
* ~~Add multiple image upload~~
* ~~Attach multiple images to a post~~
* ~~Add multiple image display in full post details~~
* Add thumbnail to post dashboard
* Add post background colour
* Add post background colours to post dashboard
* Add image scrolling to the post details
* Add image scrolling alongside text scrolling
* Add base headers to posts with no images in line with post colour
